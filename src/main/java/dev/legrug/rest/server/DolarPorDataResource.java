package dev.legrug.rest.server;

import dev.legrug.domain.dolar.Dolar;
import dev.legrug.excecoes.ExcecaoGenerica;
import dev.legrug.utils.DataUtil;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import org.eclipse.microprofile.openapi.annotations.parameters.Parameter;
import org.jboss.resteasy.annotations.jaxrs.PathParam;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.text.MessageFormat;
import java.time.LocalDate;

/**
 * End-point para recebimento de requisições de cotação do dolar por dia
 * @author Márcio Gurgel
 */
@Path("/v1/dolar/real/por-data")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class DolarPorDataResource {

    private static final Logger LOGGER = LoggerFactory.getLogger(DolarPorDataResource.class);

    @Inject
    Dolar dolar;

    @GET
    @Path("/{data}/")
    @Produces(MediaType.APPLICATION_JSON)
    public Response obtemCotacaoDoDolarPorData
             (  @PathParam("data")
                @Parameter(required = true, description = "Data-base para a cotação. Utilize o padrão yyyy-MM-dd") String data) throws ExcecaoGenerica
    {
        try
        {
            validaFormatoDaData(data);
            return Response.ok(dolar.efetuaCotacaoEmReais(LocalDate.parse(data))).build();
        }
        catch(ExcecaoGenerica excecao)
        {
            LOGGER.error(excecao);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                     .entity(excecao.getMessage()).type(MediaType.TEXT_PLAIN_TYPE).build();
        }

    }

    private boolean validaFormatoDaData(String data) throws ExcecaoGenerica
    {
        boolean formatoDataValido = DataUtil.dataEstaNoPadraoEsperado(data, DataUtil.PadraoDeData.YYYY_MM_DD);
        if(!formatoDataValido)
        {
            throw new ExcecaoGenerica(MessageFormat.format("A data recebida \"{0}\" é inválida", data));
        }
        return formatoDataValido;
    }
}