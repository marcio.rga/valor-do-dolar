package dev.legrug.rest.client;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

import javax.inject.Singleton;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import java.util.Map;

@Path("/olinda/servico/PTAX/versao/v1/odata")
@RegisterRestClient(configKey="api-cotacao-bc")
@Singleton
public interface CotacaoDoDolarBancoCentralService
{
   @GET
   @Path("/CotacaoDolarDia(dataCotacao=@dataCotacao)")
   @Produces("application/json")
   Map name(@QueryParam("@dataCotacao") String dataCotacao, @QueryParam("$format") String format);


}
