package dev.legrug.utils;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.regex.Pattern;

/**
 * Dispõe de funcionalidades utilitárias para manuseio de campos de data
 * @author Márcio gurgel
 */
public class DataUtil
{

   private DataUtil() {}

   public static boolean dataEstaNoPadraoEsperado(String dataComoString, PadraoDeData padraoDeData)
   {
      return padraoDeData.getExpressaoRegularParaValidacao().matcher(dataComoString).matches();
   }

   public static boolean ehFinalDeSemana(LocalDate data)
   {
      return data.getDayOfWeek().equals(DayOfWeek.SATURDAY) || data.getDayOfWeek().equals(DayOfWeek.SUNDAY);
   }

   /**
    * Enumeração para registro dos formatos de data aceitos pela aplicação e o padrão de expressão regular para validação
    * @author Márcio Gurgel
    */
   public enum PadraoDeData
   {

      YYYY_MM_DD(Pattern.compile("([12]\\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\\d|3[01]))"));

      private Pattern expressaoRegularParaValidacao;

      PadraoDeData(Pattern expressaoRegularParaValidacao)
      {
         this.expressaoRegularParaValidacao = expressaoRegularParaValidacao;
      }

      public Pattern getExpressaoRegularParaValidacao()
      {
         return expressaoRegularParaValidacao;
      }
   }
}
