package dev.legrug.excecoes;

/**
 * Exceção com propósitos genéricos para este sistema
 */
public class ExcecaoGenerica extends Exception
{

   public ExcecaoGenerica(String message)
   {
      super(message);
   }

}
