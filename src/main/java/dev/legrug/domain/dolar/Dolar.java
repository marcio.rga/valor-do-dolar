package dev.legrug.domain.dolar;

import dev.legrug.domain.dolar.repository.IDolarRepository;
import dev.legrug.domain.dolar.vo.ValoresDoDolarVO;
import dev.legrug.excecoes.ExcecaoGenerica;
import dev.legrug.utils.DataUtil;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import java.text.MessageFormat;
import java.time.LocalDate;

/**
 * Entidade de negócio representativa do dólar
 * @author Márcio Gurgel
 */
@RequestScoped
public class Dolar
{

   @Inject
   IDolarRepository fonteCotacaoDolarRepository;

   public ValoresDoDolarVO efetuaCotacaoEmReais(LocalDate dataParaCotacao) throws ExcecaoGenerica
   {
      validaFinalDeSemana(dataParaCotacao);
      ValoresDoDolarVO valoresDoDolarVO = fonteCotacaoDolarRepository.buscaCotacaoEmReais(dataParaCotacao);
      fonteCotacaoDolarRepository.adicionaRegistroDeAcesso(valoresDoDolarVO);
      return valoresDoDolarVO;
   }

   private void validaFinalDeSemana(LocalDate dataParaCotacao) throws ExcecaoGenerica
   {
      if(DataUtil.ehFinalDeSemana(dataParaCotacao))
      {
         throw new ExcecaoGenerica(MessageFormat.format("A data informada ({0}) é um final de semana. Informe um dia útil.", dataParaCotacao));
      }
   }

}
