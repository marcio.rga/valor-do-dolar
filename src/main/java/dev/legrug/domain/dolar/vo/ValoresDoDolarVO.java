package dev.legrug.domain.dolar.vo;

import java.math.BigDecimal;

/**
 * Value-Object para armazenamento dos valores do dólar para determinada moeda (por padrão, real)
 * @author Márcio Gurgel
 */
public class ValoresDoDolarVO
{

   public ValoresDoDolarVO(BigDecimal compra, BigDecimal venda, String dataHoraDaCotacao)
   {
      this.compra = compra;
      this.venda = venda;
      this.dataHoraDaCotacao = dataHoraDaCotacao;
   }

   private BigDecimal compra;
   private BigDecimal venda;
   private String dataHoraDaCotacao;

   public BigDecimal getCompra()
   {
      return compra;
   }

   public BigDecimal getVenda()
   {
      return venda;
   }

   public String getDataHoraDaCotacao()
   {
      return dataHoraDaCotacao;
   }

   @Override public boolean equals(Object o)
   {
      if (this == o)
         return true;
      if (!(o instanceof ValoresDoDolarVO))
         return false;

      ValoresDoDolarVO that = (ValoresDoDolarVO) o;

      if (!getCompra().equals(that.getCompra()))
         return false;
      if (!getVenda().equals(that.getVenda()))
         return false;
      return getDataHoraDaCotacao().equals(that.getDataHoraDaCotacao());
   }

   @Override public int hashCode()
   {
      return getCompra().hashCode();
   }
}

