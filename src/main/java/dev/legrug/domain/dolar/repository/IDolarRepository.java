package dev.legrug.domain.dolar.repository;

import dev.legrug.domain.dolar.vo.ValoresDoDolarVO;

import java.time.LocalDate;

/**
 * Interface para formalizar contrato com a camada de domínio, sem tornar explicito a origem dos dados.
 * De maneira que, caso a fonte dos dados mude, o domínio não será impactado.
 * @author Márcio Gurgel
 */
public interface IDolarRepository
{
   ValoresDoDolarVO buscaCotacaoEmReais(LocalDate dataParaCotacao);
   void adicionaRegistroDeAcesso(ValoresDoDolarVO valoresDoDolarVO);
}