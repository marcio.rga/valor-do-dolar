package dev.legrug.domain.dolar.repository;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;
import dev.legrug.domain.dolar.vo.ValoresDoDolarVO;
import dev.legrug.rest.client.CotacaoDoDolarBancoCentralService;
import org.bson.Document;
import org.eclipse.microprofile.rest.client.inject.RestClient;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Default;
import javax.inject.Inject;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * Implementação de repositório para acesso à dados relacionados ao domínio de {@link dev.legrug.domain.dolar.Dolar}
 *
 * @author Márcio Gurgel
 */
@ApplicationScoped
@Default
public class DolarRepository implements IDolarRepository
{

   @Inject
   @RestClient
   CotacaoDoDolarBancoCentralService cotacaoService;

   @Inject MongoClient mongoClient;

   @Override public ValoresDoDolarVO buscaCotacaoEmReais(LocalDate dataParaCotacao)
   {
      Map resposta = cotacaoService.name(formataDataParaPadraoBC(dataParaCotacao), "json");
      Map cotacoes = (Map) ((List<Object>) resposta.get("value")).get(0);

      BigDecimal cotacaoCompra = (BigDecimal) cotacoes.get("cotacaoCompra");
      BigDecimal cotacaoVenda = (BigDecimal) cotacoes.get("cotacaoVenda");
      String dataHoraDaCotacao = (String) cotacoes.get("dataHoraCotacao");

      return new ValoresDoDolarVO(cotacaoCompra, cotacaoVenda, dataHoraDaCotacao);
   }

   @Override public void adicionaRegistroDeAcesso(ValoresDoDolarVO valoresDoDolarVO)
   {
      Document document = new Document().append("uuid", UUID.randomUUID().toString())
               .append("timestamp", new Date()).append("data-e-hora-da-cotacao", valoresDoDolarVO.getDataHoraDaCotacao())
               .append("valor-de-compra", valoresDoDolarVO.getCompra()).append("valor-de-venda", valoresDoDolarVO.getVenda());
      getCollection().insertOne(document);
   }

   private MongoCollection<Document> getCollection()
   {
      return mongoClient.getDatabase("valor-do-dolar").getCollection("registro-de-acesso");
   }

   private String formataDataParaPadraoBC(LocalDate dataParaCotacao)
   {
      return new StringBuilder().append("'").append(dataParaCotacao.format(DateTimeFormatter.ofPattern("MM-dd-YYYY"))).append("'")
               .toString();
   }

}
