package dev.legrug.utils;

import org.junit.jupiter.api.Test;
import org.wildfly.common.Assert;

/**
 * Testa funcionalidades dispostas em {@link DataUtilTest}
 * @author Márcio Gurgel
 */
class DataUtilTest
{

   @Test
   void testaDataValida()
   {
      boolean dataEstaNoPadraoEsperado = DataUtil.dataEstaNoPadraoEsperado("2020-03-20", DataUtil.PadraoDeData.YYYY_MM_DD);
      Assert.assertTrue(dataEstaNoPadraoEsperado);
   }

   @Test
   void testaDataInvalida()
   {
      boolean dataEstaNoPadraoEsperado = DataUtil.dataEstaNoPadraoEsperado("01-01-2020", DataUtil.PadraoDeData.YYYY_MM_DD);
      Assert.assertFalse(dataEstaNoPadraoEsperado);
   }
}