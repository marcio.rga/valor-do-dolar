package dev.legrug.domain.dolar;

import dev.legrug.domain.dolar.vo.ValoresDoDolarVO;
import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.wildfly.common.Assert;

import javax.inject.Inject;

import java.math.BigDecimal;
import java.time.LocalDate;

/**
 * Testa funcionalidades dispostas em {@link Dolar}
 */
@QuarkusTest
class DolarTest
{

   @Inject
   Dolar dolar;

   @Test
   void efetuaCotacaoEmReais() throws Exception
   {
      ValoresDoDolarVO valoresDoDolarVO = dolar.efetuaCotacaoEmReais(LocalDate.parse("2020-03-20"));
      Assert.assertTrue(new BigDecimal("5.254").equals(valoresDoDolarVO.getCompra()));
      Assert.assertTrue(new BigDecimal("5.754").equals(valoresDoDolarVO.getVenda()));
   }

   @Test()
   void efetuaCotacaoEmReaisNoFinalDeSemana()
   {
      Assertions.assertThrows(Exception.class, () ->
         dolar.efetuaCotacaoEmReais(LocalDate.parse("2020-03-21"))
      );

   }

}