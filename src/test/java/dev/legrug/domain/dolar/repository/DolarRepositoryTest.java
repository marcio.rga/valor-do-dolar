package dev.legrug.domain.dolar.repository;

import dev.legrug.domain.dolar.vo.ValoresDoDolarVO;
import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;

import java.math.BigDecimal;
import java.time.LocalDate;

/**
 * Testa funcionalidades dispostas em {@link DolarRepository}
 */
@QuarkusTest
class DolarRepositoryTest
{

   @Inject
   DolarRepository dolarRepository;

   @Test
   void buscaCotacaoEmReais()
   {
      ValoresDoDolarVO valoresDoDolarVO = dolarRepository.buscaCotacaoEmReais(LocalDate.parse("2020-03-20"));
      Assertions.assertTrue(new ValoresDoDolarVO(new BigDecimal("5.02410"),
               new BigDecimal("5.02480"), "2020-03-20 13:03:05.621").equals(valoresDoDolarVO));
   }
}