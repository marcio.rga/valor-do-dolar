package dev.legrug.domain.dolar.mock;

import dev.legrug.domain.dolar.repository.IDolarRepository;
import dev.legrug.domain.dolar.vo.ValoresDoDolarVO;
import io.quarkus.test.Mock;

import javax.enterprise.context.ApplicationScoped;
import java.math.BigDecimal;
import java.time.LocalDate;

@Mock
@ApplicationScoped
public class DolarMock implements IDolarRepository
{

   public static final String DATA_HORA_FIXO = "2017-01-31 13:08:43.585";

   @Override public ValoresDoDolarVO buscaCotacaoEmReais(LocalDate dataParaCotacao)
   {
      return new ValoresDoDolarVO(new BigDecimal("5.254"), new BigDecimal("5.754"), DATA_HORA_FIXO);
   }

   @Override public void adicionaRegistroDeAcesso(ValoresDoDolarVO valoresDoDolarVO)
   {

   }
}
