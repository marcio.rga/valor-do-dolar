package dev.legrug.rest.server;

import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;

/**
 * Testa funcionalidades dispostas em {@link DolarPorDataResource}
 * @author Márcio Gurgel
 */
@QuarkusTest
public class DolarPorDataResourceTest {

    @Test
    public void obtemCotacaoDolar() {
        given()
          .when().get("/v1/dolar/real/por-data/{data}", "2020-03-20")
          .then()
             .statusCode(200)
             .body(is("{\"compra\":5.254,\"dataHoraDaCotacao\":\"2017-01-31 13:08:43.585\",\"venda\":5.754}"));
    }

    @Test
    public void obtemCotacaoDolar_ComDataInvalida() {
        given()
                 .when().get("/v1/dolar/real/por-data/{data}", "2020-03-")
                 .then()
                 .statusCode(500)
                 .body(is("A data recebida \"2020-03-\" é inválida"));
    }


    @Test
    public void obtemCotacaoDolar_ComDataNoFinalDeSemana() {
        given()
                 .when().get("/v1/dolar/real/por-data/{data}", "2020-03-21")
                 .then()
                 .statusCode(500)
                 .body(is("A data informada (2020-03-21) é um final de semana. Informe um dia útil."));
    }

}