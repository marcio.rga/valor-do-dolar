# valor-do-dolar project

## Acesso aos seriços (via docker):

### Swagger
http://localhost:2222/swagger

### Jaeger
http://localhost:16686


## História de usuário

Necessito criar um negócio que necessita consultar o valor do câmbio (relação dólar x real), para dias variados.
Sabe-se que o Banco Centrla dispõe de um serviço que informa esses valores. Contudo, não desejo utilizar
esse serviço. Dado que posso ter interesse em migrar para outra plataforma de consulta.
Além disso, necessito registrar os detalhes das consultas de valor de câmbio feitas, para relatórios futuros.

## Critérios de aceitação

* O serviço deve ser disposto através de uma API RESTFul
* A URL do serviço deve apresentar fácil compreensão, uso intuitivo e deve aceitar versionamento
* O serviço deve:
   * Ser desacoplado e permitir o escalonamento horizontal
   * Não deve manter estado
   * Armazenar em base de dados as consultas efetuadas no sistema
* A arquitetura deve ser flexível o suficiente para permitir troca de fonte de cotação de dólar, sem demasiado impacto nos outros componentes do sistema
* O sistema deve ter taxa de 80% de cobertura de testes

## Cenários de teste

* Data não informada
* Data inválida
* Consulta de cotação do dólar para sábado e domingo não deve ser permitida
* Em caso de erro, verificar se a menasgem de resposta é legível para o usuário da API
* Certificar de que somente o formato de data previamente definido será aceito


## Informações adicionais

Status da pipeline (compilação/testes):
[![pipeline status](https://gitlab.com/marcio.rga/valor-do-dolar/badges/master/pipeline.svg)](https://gitlab.com/marcio.rga/valor-do-dolar/-/commits/master)
